"""
Please follow these guidelines when writing callbacks:
    -   All callbacks should import the following, in this order, at minimum (whether used or not):
        (request, result, sig_classification, args).
    -   Use try:except: for all module tasks.
    -   Write a description of what the callback does, as well as what a sample callback string should look like in a
        TagCheck signature.
    -   Arguments can be passed to callbacks by using ':' to separate callback location. The arguments will be converted
        using ast.literal_eval. Therefore a list will look like: "['val', 'val2']"; string like 'val';
        dictionary like {'key1': 'val1', 'key2': 'val2"}
        Please see add_tag callback for example.
"""
from assemblyline.al.common.result import SCORE, TAG_TYPE, TAG_WEIGHT
import logging

log = logging.getLogger('assemblyline.svc.tagcheck.callbacks')


def add_tag(request, result, sig_classification, args):
    """
    Will add a defined AL Tag to result set. Expected args is a tag type:tag value string
    Example callback in signature: al_services.alsvc_tagcheck.callbacks.add_tag:['TECHNIQUE_KEYLOGGER:TypeXYZ']
    """
    try:
        for v in args:
            tag_type = v.split(':', 1)[0]
            tag_value = v.split(':', 1)[1]
            result.add_tag(TAG_TYPE[tag_type], tag_value, TAG_WEIGHT['NULL'], classification=sig_classification)
    except Exception as e:
        log.debug("add_tag callback attempt failed", e)
