#!/usr/bin/python

import csv
import sys
import pprint

from assemblyline.al.common import forge
ds = forge.get_datastore()
Classification = forge.get_classification()


def get_signatures():
    sigs = ds.get_blob('tagcheck_signatures')
    return sigs


def store_signatures(csv_path):
    with open(csv_path, 'r') as fh:
        reader = csv.reader(fh)
        sigblob = {}
        for row in reader:
            name = row[0]
            if name in sigblob:
                print "Skipping duplicate signature %s.." % name
                continue

            classification = row[1]
            if classification == 'None':
                classification = Classification.UNRESTRICTED
            status = row[2]
            if status == 'None':
                status = None
            score = row[3]
            if score == 'None':
                score = 'NULL'
            threat_actor = row[4]
            if threat_actor == 'None':
                threat_actor = None
            implant_family = row[5]
            if implant_family == 'None':
                implant_family = None
            comment = row[6]
            if comment == 'None':
                comment = None
            values = [s for s in row[7].split(";")]
            callback = row[8]
            if callback == 'None':
                callback = None

            temp_sig = {
                "classification": classification,
                "status": status,
                "score": score,
                "threat_actor": threat_actor,
                "implant_family": implant_family,
                "comment": comment,
                "values": values,
                "callback": callback,
            }

            sigblob[name] = temp_sig

    print "This is what we're about to write to riak: "
    pprint.pprint(sigblob)
    ds.save_blob('tagcheck_signatures', sigblob)
    print "Signatures written to riak."


if __name__ == "__main__":
    signature_set = get_signatures()
    # If signatures do not exist, store default signatures
    if not signature_set:
        store_signatures(sys.argv[1])



